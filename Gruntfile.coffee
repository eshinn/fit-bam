module.exports = (grunt) ->
  # Configuration
  grunt.cacheMap = []
  grunt.initConfig pkg: require './package.json'

  # Load tasks from Grunt-Tasks folder
  grunt.loadTasks 'Grunt-Tasks'

  # Register task aliases
  grunt.registerTask 'default', [
    'clean:all'
    'html:dev'
    'css:dev'
    'js:dev'
    'img:dev'
    'connect'
    'browserSync'
    'watch'
  ]

  # NOTE:
  # - Initial Grunt call runs clean:all
  # - WATCH will clean individual types
  # - WATCH COPY will be done by individual type-tasks, not globally

  # HTML
  grunt.registerTask 'html:dev', [
    'jade:dev'
    'copy:html'
  ]

  # CSS
  grunt.registerTask 'css:dev', [
    'sass:dev'
    'copy:css'
  ]

  # JS
  grunt.registerTask 'js:dev', [
    'copy:js'
  ]

  # IMG
  grunt.registerTask 'img:dev', [
    'copy:img'
  ]
