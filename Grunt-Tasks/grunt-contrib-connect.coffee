module.exports = (grunt) ->
  grunt.config 'connect',

    server:
      options:
        base: 'dist/public/'
        hostname: '*'
        port: 8080
        livereload: true

  grunt.loadNpmTasks 'grunt-contrib-connect'
