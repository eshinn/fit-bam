module.exports = (grunt) ->
  grunt.config 'browserSync',

    options:
      port: 8888
      proxy:
        target: 'localhost:8080'
        ws: false # If using WebSockets, set to 'true'
      watchTask: true
      ui:
        port: 8000
      ghostMode:
        clicks: true
        forms: true
        scroll: true

    dev:
      options:
        open: false

  grunt.loadNpmTasks 'grunt-browser-sync'
