module.exports = (grunt) ->
  grunt.config 'jade',
    options:
      basedir: 'src'
    dev:
      options:
        pretty: true
      files: [{
        cwd: 'src/pages'
        src: '**/*.jade'
        dest: 'build/public'
        expand: true
        ext: '.html'
      }]

  grunt.loadNpmTasks 'grunt-contrib-jade'
