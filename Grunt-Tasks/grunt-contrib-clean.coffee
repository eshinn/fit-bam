module.exports = (grunt) ->
  grunt.config 'clean',

    all:
      src: ['./{build,dist}']

    html:
      src: ['./{build,dist}/public/**/*.html']

    css:
      src: ['./{build,dist}/public/**/*.css']

    js:
      src: ['./{build,dist}/public/**/*.js']

    img:
      src: ['.{build,dist}/public/images/**/*.']

  grunt.loadNpmTasks 'grunt-contrib-clean'
