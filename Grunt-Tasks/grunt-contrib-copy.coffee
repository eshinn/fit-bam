module.exports = (grunt) ->
  grunt.config 'copy',

    public:
      files: [{
        expand: true
        cwd: 'build/public'
        src: '**/*'
        dest: 'dist/public/'
      }]

    html:
      files: [{
        expand: true
        cwd: 'build/public'
        src: '**/*.html'
        dest: 'dist/public/'
      }]

    css:
      files: [{
        expand: true
        cwd: 'build/public/css'
        src: '**/*.css'
        dest: 'dist/public/css/'
      }]

    js:
      files: [{
        expand: true
        cwd: 'src/js'
        src: '**/*.js'
        dest: 'dist/public/js/'
      }]

    img:
      files: [{
        expand: true
        cwd: 'src/images'
        src: '**/*.{gif,jpg,svg,png}'
        dest: 'dist/public/images/'
      }]

  grunt.loadNpmTasks 'grunt-contrib-copy'
