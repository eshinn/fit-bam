module.exports = (grunt) ->
  grunt.config 'sass',

    dev:
      options:
        style: 'expanded'
        sourcemap: 'none'
      files:
        'build/public/css/root.css': 'src/scss/root.scss'

    prod:
      files:
        'build/public/css/root.css': 'src/scss/root.scss'

  grunt.loadNpmTasks 'grunt-contrib-sass'
