module.exports = (grunt) ->
  grunt.config 'watch',

    options:
      spawn: false
      livereload: true

    jade:
      files: 'src/**/*.jade'
      tasks: [
        'clean:html'
        'html:dev'
      ]

    # css:
    #   files: 'src/stylus/**/*.{css,styl}'
    #   tasks: [
    #     'clean:css'
    #     'css:dev'
    #   ]
    css:
      files: 'src/scss/**/*.{css,scss}'
      tasks: [
        'clean:css'
        'css:dev'
      ]

    js:
      files: 'src/js/**/*.js'
      tasks: [
        'clean:js'
        'js:dev'
      ]

    img:
      files: 'src/images/**/*'
      tasks: [
        'clean:img'
        'img:dev'
      ]

  grunt.loadNpmTasks 'grunt-contrib-watch'
