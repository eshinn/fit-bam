jQuery(function() {
  var fullName;
  var email;
  var $fullName = $('.facade-form .facade-full-name');
  var $email = $('.facade-form .facade-email');
  var $poiSelect = $('#Element5297');
  var $countrySelect = $('#Element34');
  var $collegeSelect = $('#Element129');

  // Full name even handler
  $fullName.on('keyup change blur', function() {
    fullName = $(this).val().split(' ');

    // Repeat value for other name fields...
    $fullName.not(this).val($(this).val());

    // Split full name into first/last names
    $('#Element2').val(fullName.slice(0,-1).join(' '));
    $('#Element6').val(fullName.pop());
  });

  // Email event handler
  $email.on('keyup change blur', function() {
    email = $(this).val();
    // Repeat value for other email fields...
    $email.not(this).val(email);

    // Set value to BAT form email field
    $('#Element8').val(email);
  });

  // Auto-fill the rest...
  $poiSelect.val($poiSelect.find('option:contains(Aviation Management)').val());
  $countrySelect.val($countrySelect.find('option:contains(USA)').val());
  $collegeSelect.val($collegeSelect.find('option:contains(High School)').val());

  $('#Element116').val('8136216200');

  // Form Facade sumit even handler
  $('.facade-form').on('submit', function(e) {
    e.preventDefault();
    // alert(e)
    // return false;
    $('#LeadSystem').submit();
  });

  // Set facade-form 1 as target link
  $('.thank-you').first().attr('id', 'thank-you');
  $('.facade-form').first().attr('id', 'target-form');

  // Show/hide form/ThankYou
  if (/Element2=/i.test(window.location.search)) {
    // alert("Got Lead");
    $('.thank-you').toggleClass('is-hidden');
    window.location.hash = "thank-you"
  } else {
    $('.facade-form').toggleClass('is-hidden');
    $('#returnUrl').val(window.location.toString().replace(new RegExp(window.location.hash, 'i'),''));
  }
});
